# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

# download all files from azure container
# read each file and break it into lines
# buld the overall input for mapreduce
# 
# list of [<offset, line string>, ...] key-value pairs
def main(name: str) -> list:
    blob_service_client = BlobServiceClient.from_connection_string("DefaultEndpointsProtocol=https;AccountName=durablefunctionpasocloud;AccountKey=t1F0asCwL0KeUt0CS3Z8oA7JCyuxTpHksegF3lY0XDbOnEol6ptJnNdukeyoC3foAAbVjo16s1Cd+AStrMffQQ==;EndpointSuffix=core.windows.net")
    container_client = blob_service_client.get_container_client(container=name) 
    
    result = []
    for blob in container_client.list_blobs():
        #logging.error("Blob: " + str(blob.name))
        file = container_client.download_blob(blob.name).readall()
        lines = file.splitlines()
        linesNumerated = []
        i = 0
        for line in lines:
            i = i + 1
            linesNumerated.append((i, str(line)))
        result.extend(linesNumerated)

    return result
