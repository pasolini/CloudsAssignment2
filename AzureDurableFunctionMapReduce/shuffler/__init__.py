# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(wordCntList: list) -> dict:
    result={}
    for word_cnt in wordCntList:
        (w, c) = word_cnt
        cnts = result.get(w)
        if cnts is None:
            cnts = [c]
        else:
            cnts.append(c)
        result[w] = cnts
    return result
