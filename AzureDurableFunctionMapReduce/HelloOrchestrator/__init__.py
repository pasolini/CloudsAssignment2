# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):

    '''full_text: str = context.get_input()

    if not full_text:
        raise Exception("A text is required as input in post")

    full_text = ciaone gino mi fa piacere
    riverderti qui sta sera
    perchè non sono scemo
    ma mi piace il cibo
    forse cibo vegano
    ciao gino sto bene'''

    #logging.error("Get datas")
    full_text = yield context.call_activity("GetInputDataFn", "song-to-read")
    #logging.error("Data read" + str(full_text))

    mapperResults = [ context.call_activity("mapper", i) for i in full_text]
    #logging.error("mapperResults" + str(mapperResults))
    mapperWaitedRes = yield context.task_all(mapperResults)
    #logging.error("mapperWaitedRes" + str(mapperWaitedRes))
    mapperWaitedRes = [ris for list in mapperWaitedRes for ris in list]
    #logging.error("mapperWaitedRes second way" + str(mapperWaitedRes))

    shuffler = yield context.call_activity("shuffler", mapperWaitedRes)
    #logging.error("shuffler" + str(shuffler))
    
    reducerResults = [ context.call_activity("reducer", (word, shuffler[word])) for word in shuffler]
    #logging.error("reducerResults" + str(reducerResults))
    results = yield context.task_all(reducerResults)
    #logging.error("final results" + str(results))

    return results

    '''files = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    tasks = []
    for file in files:
        tasks.append(context.call_activity("E2_CopyFileToBlob", file))
    
    results = yield context.task_all(tasks)
    total_bytes = sum(results)'''

    return [result1, result2, result3, result4]

main = df.Orchestrator.create(orchestrator_function)