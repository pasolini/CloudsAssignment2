import logging

import azure.functions as func
from math import sin

Ns = [10, 100, 1000, 10000, 100000, 1000000]

def numericalIntegration(parts, lowerBound, upperBound):
	
	sections = [lowerBound + x * (upperBound - lowerBound) / parts for x in range(parts + 1)]
	#print(sections)
	result = 0.0
	#print(len(sections))
	for index in range(len(sections) - 1):

		#print("===========================")
		x = sections[index]
		#print(x)
		y = sections[index + 1]
		#print(y)
		di = abs(sin(x))
		#print(di)
		result += (di * abs(x - y))
		#print(di)
	return result

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    start = req.params.get('start')
    end = req.params.get('end')

    if not start and not end:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            start = req_body.get('start')
            end = req_body.get('end')

    if start and end:

        for N in Ns:
            res = numericalIntegration(N, float(start), float(end))
            print("result: " + str(res) + " Points: " + str(N))

        return func.HttpResponse(f"Finished, {start} , {end}. Numerical integration done.")
    else:
        return func.HttpResponse(
             "waiting an url similar to: https://pasoliniintegration10.azurewebsites.net/api/httpexample?start=0&end=3.14159",
             status_code=200
        )
